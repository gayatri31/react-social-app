import React, { Component } from 'react';
import logo from '../logo.svg';

class Header extends Component {
  render() {
    return (<div>

      <nav className="navbar navbar-expand-lg navbar-dark bg-primary rounded">
        <div className="container">
          <a className="navbar-brand" href="#"><img src={logo} className="App-logo" alt="logo" /></a>
          {/* <ul className="navbar-nav mr-auto">
            <li className="nav-item active">
              <a className="nav-link">Left Link 1</a>
            </li>
            <li className="nav-item">
              <a className="nav-link">Left Link 2</a>
            </li>
          </ul> */}
          <ul className="navbar-nav ml-auto">
            <li className="nav-item">
              <a className="nav-link"><i class="fas fa-th-large"></i> ALL Adds</a>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span>Vinay <i class="fas fa-user"></i></span></a>
              <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                <a className="dropdown-item" href="#"><span><i class="fas fa-home"></i>Personal Home</span></a>
                <a className="dropdown-item" href="#"><span><i class="fas fa-th-large"></i> My ads</span></a>
                <a className="dropdown-item" href="#"><span><i class="fas fa-heart"></i> My Fav</span></a>
                <a className="dropdown-item" href="#"><span><i class="fas fa-star"></i>Saved Search</span></a>
              </div>
            </li>
          </ul>
        </div>

      </nav>
    </div>);
  }
}

export default Header;
